import * as ActionTypes from './ActionTypes'

export const addComents = (dishId, rating, auther, comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: {
        dishId: dishId,
        rating: rating,
        auther: auther,
        comment: comment
    }
});