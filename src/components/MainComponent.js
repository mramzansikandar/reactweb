import React, { Component } from 'react';
import Menu from './MenuComponent';
import Dishdetails from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeCompoment';
import contact from './contactComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addComents } from "../redux/ActionCreaters";

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.Promotions,
    leaders: state.Leaders
  }
}

const mapDispatchToProps = dispatch => ({
  addComents: (dishId, rating, author, comment) => dispatch(addComents(dishId, rating, author, comment))
});

class Main extends Component {
constructor(props) {
  super(props);
}



onDishSelect(dishId) {
    this.setState({ selectedDish: dishId});
}


  render() {
    const HomePage = () => {
      return(
            <Home 
                dish={this.props.dishes.filter((dish) => dish.featured)[0]}
                promotion={this.props.promotions.filter((promo) => promo.featured)[0]}
                leader={this.props.leaders.filter((leader) => leader.featured)[0]}
            />
        );
    }

    return (
      <div>      
        <Header/>  
        <Switch>
          <Route path="/home" component={HomePage} />
          <Route exact path="/menu" component={() => <Menu dishes = {this.props.dishes} /> } />
          <Route exact path="/contactus" component={contact}/>
          <Redirect to="/home>" />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Main));
